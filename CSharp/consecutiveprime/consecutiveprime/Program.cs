﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
public class Program
{
    public static void Main()
    {
        int[] a = new int[100000];
        int n = Convert.ToInt32(Console.ReadLine());
        int p = 2, count = 0, k = 0, sum = 2, i;
        for (i = 2; i <= n; i++)
        {
            a[i] = 0;
        }
        for (p = 2; p * p <= n; p++)
        {
            if (a[p] != 1)
            {
                for (i = p * 2; i <= n; i += p)
                {
                    a[i] = 1;
                }
            }
        }

        for (k = 3; k <= (n / 2); k += 2)
        {
            if (a[k] == 0)
            {
                sum = sum + k;
                if (sum <= n && a[sum] == 0)
                {
                    count++;
                    Console.Write(sum + " ");
                }
            }
        }   Console.WriteLine();
        Console.WriteLine("count is:" + count);
        Console.ReadKey();
    }
}
